#!/usr/bin/perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -strict;
use Mojo::Collection 'c';
use Mojo::DOM;
use Mojo::File;
use Mojo::URL;
use Mojo::UserAgent;
use Mojo::Util qw(decode encode url_unescape);
use Mojo::JSON qw(decode_json encode_json);
use Mojolicious;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use DateTime;
use DateTime::Format::RFC3339;
use DateTime::Locale;
use File::Copy::Recursive qw(dircopy);
use File::Spec qw(catfile);
use File::Path qw(rmtree);
use FindBin qw($Bin);
use Text::Slugify 'slugify';
use XML::Atom::SimpleFeed;
use Pandoc;
use Scalar::Util qw(reftype);
use POSIX;
use lib '.';

use vars qw($lc_loaded);
BEGIN {
    eval "use LastCustom";
    if ($@) {
        $lc_loaded = 0;
    } else {
        $lc_loaded = 1;
    }
}

# Get config
my $m      = Mojolicious->new();
my $ua     = Mojo::UserAgent->new();
my $config = $m->plugin(Config => {
    file    => File::Spec->catfile($Bin, 'last.conf'),
    default => {
        theme       => 'default',
        sort        => 1,
        license     => '',
        markdown    => 0,
        description => '',
    }
});
my $theme      = $config->{theme};
my $last_black_cat_effect;

# Copy theme
rmtree("$Bin/public");
dircopy("$Bin/themes/$theme", "$Bin/public");

# Start to modify the index file
my $url     = Mojo::URL->new($config->{public_url});
my $file    = Mojo::File->new('public/index.html');
my $license = $config->{license};
my $index   = open_index();

## Atom feed
   $url  = Mojo::URL->new($config->{public_url});
my $iso  = DateTime::Format::RFC3339->format_datetime(DateTime->now());
my $feed = XML::Atom::SimpleFeed->new(
    title   => $config->{title},
    id      => $url->to_string,
    updated => $iso,
    author  => $config->{author},
    link    => {
        rel  => 'self',
        href => $url->path($url->path->merge('feed.atom'))->to_string
    },
    '-encoding' => 'utf-8'
);

## (re)Initialize variables
$url = Mojo::URL->new($config->{public_url});
my (@entries);
my $regex;
   $regex = join('|', @{$config->{hidden_tags}}) if $config->{hidden_tags};
### Pagination
my $pagination = $config->{pagination} || 0;
   $pagination = 0 unless $pagination > 0;
   $pagination = floor($pagination);
my $page       = 1;
### Where to insert the toots
my $c = $index->find('#content')->first;
### Date formatting stuff
my $f = DateTime::Format::RFC3339->new();
my $l = DateTime::Locale->load($config->{language});
### Sort URLs
my $urls = c(@{$config->{urls}});
   $urls = $urls->sort(
    sub {
        my $a2 = $a;
        $a2 =~ s#^.*/([^/]+)$#$1#;
        my $b2 = $b;
        $b2 =~ s#^.*/([^/]+)$#$1#;
        $a2 <=> $b2
    }
) if $config->{sort};
   $urls  = $urls->reverse if $config->{reverse};
## Create needed dirs
mkdir 'cache' unless -e 'cache';

## URLs processing
# fetching toots, attachments, formatting, creating html, atom feed
my $n    = 0;
my %versions = ();
$urls->each(
    sub {
        my ($e, $nb) = @_;
        my ($content, $i, $date, $time, $attachments, $id, $dt, $num, $attachments2) = process_entry($e, $nb);
        format_and_insert_toot($content, $i, $date, $time, $attachments, $id, $dt, $num, $attachments2);
    }
);

$c->append_content("               ");

# Write the index.html file
$file->spurt(encode('UTF-8', $index));

# Change title on all pages
c(glob('public/*.html'))->each(
    sub {
        my ($e, $nb) = @_;
        my $html_file = Mojo::File->new($e);
        my $i = Mojo::DOM->new(decode('UTF-8', $html_file->slurp));
        $i->find('title')
              ->first
              ->content(sprintf('%d days since last black cat effect.', $last_black_cat_effect));
        $i->find('meta[property="og:title"]')
              ->first
              ->attr(content => sprintf('%d days since last black cat effect.', $last_black_cat_effect));
        $i->find('#header-title')
              ->first
              ->content(sprintf('%d days since last black cat effect.', $last_black_cat_effect));
        $html_file->spurt(encode('UTF-8', $i));
});

# Add entries in the atom feed
my $en = c(@entries);
   $en = $en->reverse unless $config->{reverse};
my $max = ($en->size < 20) ? $en->size -1 : 19;
   $en = c(@{$en->to_array}[0 .. $max]);
$en->each(
    sub {
        my ($e, $num) = @_;
        $feed->add_entry(
            title   => $e->{title},
            id      => Mojo::URL->new($config->{public_url})->to_string.'#'.$e->{id},
            updated => $e->{updated},
            content => $e->{content},
        );
    }
);
## Write the atom feed
Mojo::File->new('public/feed.atom')->spurt($feed->as_string);

sub open_index {
    my $page        = shift // '/index.html';
    my $description = shift;

    my $file   = Mojo::File->new("$Bin/themes/$theme/index.html");
    my $index  = Mojo::DOM->new(decode('UTF-8', $file->slurp));
    $index->find('html')
          ->first
          ->attr(lang => $config->{language});
    $index->find('meta[name="description"]')
          ->first
          ->attr(content => $config->{description});
    $index->find('meta[name="author"]')
          ->first
          ->attr(content => $config->{author});
    my $public_url = $config->{public_url};
    $public_url =~ s#/$##;
    $index->find('meta[property="og:url"]')
          ->first
          ->attr(content => $public_url.$page);
    $index->find('meta[property="og:image"]')
          ->first
          ->attr(content => $public_url.'/img/favicon.png');
    if ($description) {
        $index->find('meta[property="og:description"]')
              ->first
              ->attr(content => $description);
    } else {
        $index->find('meta[property="og:description"]')
              ->first
              ->remove;
    }
    $index->find('link[rel="alternate"]')
          ->first
          ->attr(href => $url->path($url->path->merge('feed.atom'))->to_string);
    $index->find('#author')
          ->first
          ->content($config->{author});
    if ($license) {
        $license = "<img alt=\"$license\" src=\"img/$license.png\">" if ($license eq 'cc-0');
        $license = "<img alt=\"$license\" src=\"img/$license.png\">" if ($license eq 'public-domain');
        $license = "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-nd/4.0/\"><img alt=\"$license\" src=\"img/$license.png\"></a>" if ($license eq 'cc-by-nc-nd');
        $license = "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by-nc-sa/4.0/\"><img alt=\"$license\" src=\"img/$license.png\"></a>" if ($license eq 'cc-by-nc-sa');
        $license = "<a rel=\"license\" href=\"https://creativecommons.org/licenses/by-nd/4.0/\"><img alt=\"$license\" src=\"img/$license.png\"></a>"   if ($license eq 'cc-by-nd');
        $license = "<a rel=\"license\" href=\"https://creativecommons.org/licenses/by/4.0/\"><img alt=\"$license\" src=\"img/$license.png\"></a>"      if ($license eq 'cc-by');
        $license = "<a rel=\"license\" href=\"https://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"$license\" src=\"img/$license.png\"></a>"   if ($license eq 'cc-by-sa');

        $index->find('#license')
              ->first
              ->content($license);
    } else {
        $index->find('#license')
              ->first
              ->remove;
    }

    return $index;
}

sub process_entry {
    my ($i, $num) = @_;

    if (reftype(\$i) eq 'SCALAR') {
        ## Get the ID of the toot
        my $id = $i;
           $id =~ s#^.*/([^/]+)$#$1#;

        ## Get only the pod's hostname and scheme
        my $host = Mojo::URL->new($i)->path('');

        my $cache = (-e "cache/$id.json");

        my $msg   = "Processing $i";
           $msg  .= " from cache" if $cache;
        say $msg;

        my $res;
           $res = $ua->get($host->path("/api/v1/statuses/$id"))->result unless $cache;
        if ($cache || $res->is_success) {
            Mojo::File->new("cache/$id.json")->spurt(encode_json($res->json)) unless $cache;
            ## Get only the targeted toot, not the replies
            my $json    = ($cache) ? decode_json(Mojo::File->new("cache/$id.json")->slurp) : $res->json;

            my ($content, $dt, $attach) = extract_from_json($json);

            $content = filter_content($content);

            my ($date, $time) = format_time($dt);

            my ($attachments, $attachments2) = fetch_attachments($attach, $num);

            ## Mix date, time, content and attachments
            # Modifs persos
            if ($lc_loaded) {
                $content = LastCustom::custom_filter($content, $id);
            }

            return ($content, $i, $date, $time, $attachments, $id, $dt, $num, $attachments2);
        } elsif ($res->is_error) {
            die "Error while fetching $i: $res->message";
        }
    } else {
        my ($content, $attachments, $attachments2, $date, $time, $id, $dt) = ('', '', '');
        c(@{$i})->each(
            sub {
                my ($e, $nb) = @_;

                my ($tmp_content, $tmp_i, $tmp_date, $tmp_time, $tmp_attachments, $tmp_id, $tmp_dt, $tmp_num, $tmp_attachments2) = process_entry($e, $nb);
                if ($nb == 1) {
                    $i            = $tmp_i;
                    $date         = $tmp_date;
                    $time         = $tmp_time;
                    $id           = $tmp_id;
                    $dt           = $tmp_dt;
                    $content      = $tmp_content;
                    $attachments  = $tmp_attachments;
                    $attachments2 = $tmp_attachments2;
                } else {
                    if ($tmp_content) {
                        my $tmp_dom   = $tmp_content;
                        my $dom       = $content;
                        $tmp_dom      =~ s#(^<p>|</p>$)##g;
                        $dom          =~ s#(^<p>|</p>$)##g;
                        $content      = Mojo::DOM->new("<p>$dom<br>$tmp_dom</p>");
                    }
                    if ($attachments) {
                        my $tmp_dom   = Mojo::DOM->new($tmp_attachments)->at('p')->content;
                        my $dom       = Mojo::DOM->new($attachments)->at('p')->content;
                        $attachments  = Mojo::DOM->new($attachments)->at('p')->replace("<p>$dom$tmp_dom</p>");
                    }
                    if ($attachments2) {
                        my $tmp_dom   = Mojo::DOM->new($tmp_attachments2)->at('p')->content;
                        my $dom       = Mojo::DOM->new($attachments2)->at('p')->content;
                        $attachments2 = Mojo::DOM->new($attachments2)->at('p')->replace("<p>$dom$tmp_dom</p>");
                    }
                }
            }
        );
        return ($content, $i, $date, $time, $attachments, $id, $dt, $num, $attachments2);
    }
}

sub extract_from_json {
    my $json = shift;

    ## Get the content of the toot
    my $content = Mojo::DOM->new($json->{content});
    ## Get the metadata (ie date and time) of the toot
    my $dt      = $f->parse_datetime($json->{created_at})->set_locale($config->{language});
    $dt->set_time_zone($config->{timezone}) if defined $config->{timezone};
    ## Get the attachments of the toot
    my $attach = c(@{$json->{media_attachments}});

    my $tmp = DateTime->now()->delta_days($dt);
    $last_black_cat_effect = $tmp->days() if (!defined($last_black_cat_effect) || $tmp->days() < $last_black_cat_effect);
    return ($content, $dt, $attach);
}

sub filter_content {
    my $content = shift;

    ## Remove style attribute
    ## Replace emoji with unicode characters
    $content->find('img.emojione')->each(
        sub {
            my ($e, $num) = @_;
            $e->replace($e->attr('alt'));
        }
    );
    ## Remove the configured hashtags
    $content->find('a.mention.hashtag')->each(
        sub {
            my ($e, $num) = @_;
            my $href = $e->attr('href');
            $e->remove if $href =~ m#tags/($regex)$#;
        }
    ) if $regex;

    return $content;
}

sub format_time {
    my $dt = shift;

    ## Format date and time
    my $date    = $dt->format_cldr($l->date_format_full);
    my $time    = $dt->format_cldr($l->time_format_medium);

    return ($date, $time),
}

sub fetch_attachments {
    my ($attach, $num) = @_;

    ## Store the attached images locally
    my @imgs;
    my @imgs2;
    $attach->each(
        sub {
            my ($e, $inum) = @_;

            my $src = $e->{url};
               $src =~ s#\?(.*)##;
            my $q   = $1;
               $src =~ m#/([^/]*)$#;
            my $n   = $1;
               $src = Mojo::URL->new($src);

            ## Attachments cache
            my $acache = (-e "cache/img/$n" && -e "cache/img/$n.meta");
            my $msg = sprintf("  Fetching image: %s", $src);
               $msg .= " from cache" if $acache;
            say $msg;

            my $img = "img/$n";;
            my $rmime;
            if ($acache) {
                # Get file metadata from cache
                $rmime = decode_json(Mojo::File->new("cache/$img.meta")->slurp);
            } else {
                my $r = $ua->get($src)->result;
                if ($r->is_success) {
                    my $body = $r->body;
                      $rmime = $r->headers->content_type;

                    # Create cache
                    mkdir 'cache/img' unless -d 'cache/img';
                    Mojo::File->new("cache/$img")->spurt($body);
                    Mojo::File->new("cache/$img.meta")->spurt(encode_json($rmime));

                    # Copy file
                    Mojo::File->new("public/$img")->spurt($body);
                } elsif ($r->is_error) {
                    die sprintf("Error while fetching %s: %s", $src, $m->dumper($r->message));
                }
            }
            if ($e->{type} eq 'image') {
                push @imgs, "<img class=\"u-max-full-width\" src=\"$img\" alt=\"\">";
                push @imgs2, "<img class=\"u-max-full-width\" src=\"../$img\" alt=\"\" />";
            } elsif ($e->{type} eq 'video') {
                push @imgs, "<video class=\"u-max-full-width\" src=\"$img\" alt=\"\"></video>";
                push @imgs2, "<video class=\"u-max-full-width\" src=\"../$img\" alt=\"\" ></video>";
            }
        }
    );
    my $attachments  = (scalar @imgs)  ? "<div><p>@imgs</p></div>"  : '';
    my $attachments2 = (scalar @imgs2) ? "<div><p>@imgs2</p></div>" : '';

    return ($attachments, $attachments2);
}

sub format_and_insert_toot {
    my ($content, $i, $date, $time, $attachments, $id, $dt, $num, $attachments2) = @_;
    $content =~ s#<p></p>|</br>##g;
    $content =~ s#<br>#<br />#g;
    my $append = <<EOF;

                    <article>
                        <a href="$id.html">
                            <h2>$date <span style="font-size: 0.5em;">$time</span></h2>
                        </a>
                        $content
                        $attachments
                        <div>
                            <a href="$i">
                                <em>Source</em>
                            </a>
                        </div>
                    </article>
                    <hr>

EOF

    ## Insert the toot
    $c->append_content($append);

    ## Toot page
    my $description = Mojo::DOM->new($content)->all_text;
    my $tpindex = open_index('/'.$id.'.html', $description);
    my $tpc     = $tpindex->find('#content')->first;
    $tpc->append_content($append);
    Mojo::File->new("public/$id.html")->spurt(encode('UTF-8', $tpindex));

    ## Paginate
    if ($pagination && (($num % $pagination) == 0 || ($num == $urls->size))) {
        my $prec = ($page == 2) ? 'index' : 'page'.($page - 1);

        if ($num == $urls->size) {
            $c->append_content("\n                    <p class=\"u-pull-left\"><a class=\"button button-primary\" href=\"$prec.html\">⇐ Page précédente</a></p>");
        } else {
            $page++;
            if ($page == 2) {
                $c->append_content("\n                    <p class=\"u-pull-right\"><a class=\"button button-primary\" href=\"page$page.html\">Page suivante ⇒</a></p>");
            } else {
                $c->append_content("\n                    <p class=\"u-pull-left\"><a class=\"button button-primary\" href=\"$prec.html\">⇐ Page précédente</a></p><p class=\"u-pull-right\"><a class=\"button button-primary\" href=\"page$page.html\">Page suivante ⇒</a></p>");
            }
            $c->append_content("               ");

            $file->spurt(encode('UTF-8', $index));

            $file  = Mojo::File->new("public/page$page.html");
            $index = open_index('/page'.$page.'.html');
            $c     = $index->find('#content')->first;
        }
    }

    ## Create entries for the atom feed
    push @entries, {
        title   => "$date $time",
        id      => "$id",
        updated => DateTime::Format::RFC3339->format_datetime($dt),
        content => $append
    };
}
